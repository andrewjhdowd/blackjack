const MDCTextField = mdc.textField.MDCTextField;
const textFields = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
);

const MDCCheckbox = mdc.checkbox.MDCCheckbox;
const checkboxes = [].map.call(
  document.querySelectorAll(".mdc-checkbox"),
  function (el) {
    return new MDCCheckbox(el);
  }
);

const submitButton = document.getElementById('sign-up');
const signUpForm = document.forms[0];

const fullnameInput = signUpForm[0];
const usernameInput = signUpForm[1];
const passwordInput = signUpForm[2];
const passwordconfirmationInput = signUpForm[3];
const ageInput = signUpForm[4];
const birthdateInput = signUpForm[5];
const legalCheckbox = signUpForm[6];
const termsCheckbox= signUpForm[7];
    
const signUpFormInputs = [
    fullnameInput,
    usernameInput,
    passwordInput,
    passwordconfirmationInput,
    ageInput,
    birthdateInput,
];

function displayInfo(signUpFormInputs) {

  console.log("Full Name: " + fullnameInput.value);
  console.log("Username: " + usernameInput.value);
  console.log("Enter Password: " + passwordInput.value);
  console.log("Confirm Password: " + passwordconfirmationInput.value);
  console.log("Age: " + ageInput.value);
  console.log("Birth Date: " + birthdateInput.value);

}

function legalCheckboxChecked() {
    return legalCheckbox.checked;
}

function termsCheckboxChecked() {
    return termsCheckbox.checked;
}

function comparePasswords(){
    return passwordInput.value === passwordconfirmationInput.value;
}

function isNotEmpty(input){
    return input.value !== "";
}

function validateFields(){
    return signUpFormInputs.every(isNotEmpty);
}

function ageVerification() {
  return ageInput.value >= 13;
}

function onSignUp(){
    event.preventDefault();
    //console.log(validateFields());
    //console.log(comparePasswords());
    //console.log(ageVerification());
    //console.log(legalCheckboxChecked());
    //console.log(termsCheckboxChecked());

    if (legalCheckboxChecked() == true && termsCheckboxChecked() == true && comparePasswords() == true && validateFields() == true && ageVerification() == true){
        displayInfo();
        console.log("The user is eligible")
        console.log("The user has checked the legal checkbox");
        console.log("The user has checked the terms checkbox");
    }
    else{
        displayInfo();
        console.log("The user is ineligible");
        if(legalCheckboxChecked() == true){
          console.log("The user has checked the legal checkbox");
        }
        else{
          console.log("The user has not checked the legal checkbox");
        }

        if(termsCheckboxChecked() == true){
          console.log("The user has checked the terms checkbox");
        }
        else{
          console.log("The user has not checked the terms checkbox");
        }
    }

    //console.log(signUpForm[0].value);
    //console.log(legalCheckboxChecked())
    //console.log(comparePasswords());
    //console.log("button pressed");
}

submitButton.addEventListener("click", onSignUp);

//console.log(mdc);





